<?php
/**
 * The WordPress Plugin Boilerplate.
 *
 * A foundation off of which to build well-documented WordPress plugins that also follow
 * WordPress coding standards and PHP best practices.
 *
 * @package   JSG_Users
 * @author    Matt Stvartak <matt@theideapeople.com
 * @license   GPL-2.0+
 * @link      http://www.theideapeople.com
 * @copyright 2013 © The Idea People
 *
 * @wordpress-plugin
 * Plugin Name: JumpShot Genie® User Connections
 * Plugin URI:  http://www.theideapeople.com
 * Description: Connects different user types for <strong>Jumpshot</strong>Genie®
 * Version:     1.0.0
 * Text Domain: jsg-admin
 * Author:      The Idea People
 * Author URI:  http://www.theideapeople.com
 * License:     GPL-2.0+
 * Domain Path: /lang
 *
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'JSGUSERS_PATH', dirname(__FILE__) );
define( 'JSGUSERS_URL', plugins_url() . '/jsgadmintheme' );

// TODO: replace `class-plugin-name.php` with the name of the actual plugin's class file
require_once( plugin_dir_path( __FILE__ ) . 'includes/users.php' );

// Register hooks that are fired when the plugin is activated, deactivated, and uninstalled, respectively.
// TODO: replace Plugin_Name with the name of the plugin defined in `class-plugin-name.php`
register_activation_hook( __FILE__, array( 'JSG_Users', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'JSG_Users', 'deactivate' ) );

new JSG_Users();