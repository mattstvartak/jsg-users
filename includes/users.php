<?php
/**
 * JumpShot Genie® Admin Theme
 *
 * @package   JSG_Users
 * @author    Matt Stvartak <matt@theideapeople.com>
 * @license   GPL-2.0+
 * @link      http://www.theideapeople.com
 * @copyright 2013 © The Idea People
 */

/**
 * JumpShot Genie® Admin Theme Class
 *
 * @package JSG_Users
 * @author  Matt Stvartak <matt@theideapeople.com>
 */
class JSG_Users {

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since     1.0.0
	 */
	public function __construct() {
		add_action( 'show_user_profile', array(&$this, 'add_custom_user_meta') );
		add_action( 'edit_user_profile', array(&$this, 'add_custom_user_meta') );
		add_action( 'personal_options_update', array(&$this, 'save_custom_user_meta') );
		add_action( 'edit_user_profile_update', array(&$this, 'save_custom_user_meta') );
		add_action( 'parse_query', array(&$this, 'restrict_view') );
	}

		/**
	 * Run these actions on init
	 *
	 * @since  1.0.0
	 */
	public static function activate() {
		self::add_user_roles();
	}

	/**
	 * Deactivate
	 *
	 * @since  1.0.0
	 */
	public static function deactivate() {
		remove_role( 'player' );
		remove_role( 'coach' );
	}

	/**
	 * Add custom fields to the user profiles based on user role
	 *
	 * @since  1.0.0
	 */
	public function add_custom_user_meta($user) {
		if ( in_array( 'player', $user->roles ) ):
		?>
		<h3>Additional Profile Information</h3>

		<table class="form-table">
			<tr>
				<th><label for="coach">Assigned Coach</label></th>
				<td>
					<select id="coach" name="coach">
						<option value="null"></option>
						<?php
						$users = get_users( array( 'role' => 'coach' ) );
						$coach = get_the_author_meta( 'coach', $user->ID );

						foreach( $users as $user) {
							if ( $coach == $user->ID ) {
								echo '<option value="' . $user->ID . '" selected>';
							} else {
								echo '<option value="' . $user->ID . '" >';
							}
							echo $user->user_firstname . ' ' . $user->user_lastname;
							echo '</option>';
						}
						?>
					</select>
					<span class="description">Select a coach to assign to this player.</span>
				</td>
			</tr>
		</table>
		<?php endif;
	}

	/**
	 * Saves the user meta information to the database
	 *
	 * @since  1.0.0
	 */
	public function save_custom_user_meta($user_id) {
		global $user;
		if( !current_user_can( 'edit_user', $user_id ) && in_array( 'player', $user->roles ))
			return false;

		if ( isset($_POST['coach']) )
			update_usermeta( $user_id, 'coach', $_POST['coach'] );
	}

	/**
	 * Generate new roles 'player' and 'coach'
	 *
	 * @since  1.0.0
	 */
	public static function add_user_roles() {
		$editor = get_role( 'editor' );
		$administrator = get_role( 'administrator' );

		$player_caps = array(
			'read' => true,
			'edit_posts' => true,
			'upload_files' => true
		);

		$player = add_role( 'player', 'Player', $player_caps );
		$coach = add_role( 'coach', 'Coach', $editor->capabilities );
	}

	/**
	 * Restrict view based on user role.
	 * 	Coach: See players posts assigned to them.
	 * 	Player: See only personal posts.
	 *
	 * @return object $query object
	 */
	public function restrict_view($wp_query) {
		global $current_user;

		if ( in_array( 'coach', $current_user->roles ) && ! in_array( 'administrator', $current_user->roles ) ) {
			$players = get_users( array('meta_key' => 'coach', 'meta_value' => $current_user->ID) );
			$player_ids = array();

			foreach ( $players as $player ) {
				$player_ids[] = $player->data->ID;
			}

			$wp_query->set('author', implode(', ', $player_ids) );
		} else if ( in_array( 'player', $current_user->roles ) ) {
			$wp_query->set('author', $current_user->ID);
		}
	}
}
?>